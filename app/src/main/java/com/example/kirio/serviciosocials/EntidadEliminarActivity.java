package com.example.kirio.serviciosocials;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class EntidadEliminarActivity extends Activity {
    EditText editCodigo_org;
    ControlBD controlhelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entidad_eliminar);
        controlhelper=new ControlBD (this);
        editCodigo_org=(EditText)findViewById(R.id.editCodigo_org);
    }
    public void eliminarEntidad(View v){
        String vacio="";
        String ed1=editCodigo_org.getText().toString();
        if(vacio.equalsIgnoreCase(ed1)) {
            Toast.makeText(this, "Ingrese el codigo de la entidad", Toast.LENGTH_LONG).show();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setTitle("Eliminar");
            dialog.setMessage("SE ELIMINARAN EN CASCADA TODOS LOS REGISTROS RELACIONADOS. Realmente desea eliminar este Registro?");
            dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aceptar();
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar();
                }
            });
            dialog.show();
        }
    }

    public void aceptar() {
        String regEliminadas;
        Entidad entidad=new Entidad();
        entidad.setCodigo_org(editCodigo_org.getText().toString());
        try {
            controlhelper.abrir();
            regEliminadas=controlhelper.eliminar(entidad);
            controlhelper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            controlhelper.cerrar();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar() {
        finish();
    }




}
