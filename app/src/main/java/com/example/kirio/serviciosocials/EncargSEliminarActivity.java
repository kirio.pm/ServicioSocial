package com.example.kirio.serviciosocials;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EncargSEliminarActivity extends Activity {
    EditText editCodencarg;
    ControlBD controlhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encarg_seliminar);
        controlhelper=new ControlBD (this);
        editCodencarg=(EditText)findViewById(R.id.editCodencarg);
    }
    public void eliminarEncargS(View v){
        String vacio="";
        String ed1=editCodencarg.getText().toString();
        if(vacio.equalsIgnoreCase(ed1)) {
            Toast.makeText(this, "Ingrese el codigo del Encargado del Servicio Social", Toast.LENGTH_LONG).show();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setTitle("Eliminar");
            dialog.setMessage("SE ELIMINARAN EN CASCADA TODOS LOS REGISTROS RELACIONADOS. Realmente desea eliminar este Registro?");
            dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aceptar();
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar();
                }
            });
            dialog.show();
        }
    }

    public void aceptar() {
        String regEliminadas;
        EncargS encargS = new EncargS();
        encargS.setCodencarg(editCodencarg.getText().toString());
        try {
            controlhelper.abrir();
            regEliminadas = controlhelper.eliminar(encargS);
            controlhelper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            controlhelper.cerrar();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar() {
        finish();
    }


    public void limpiar(View view){
        editCodencarg.setText("");
    }
}