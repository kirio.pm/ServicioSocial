package com.example.kirio.serviciosocials;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.SQLException;

public class ModalidadEliminarActivity extends Activity {
    EditText editcodModalidad;
    ControlBD controlhelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modalidad_eliminar);
        controlhelper=new ControlBD (this);
        editcodModalidad=(EditText)findViewById(R.id.editcodModalidad);
    }

    public void eliminarModalidad(View v) throws SQLException {
        String vacio="";
        String ed1=editcodModalidad.getText().toString();
        if(vacio.equalsIgnoreCase(ed1)) {
            Toast.makeText(this, "Ingrese el codigo de la modalidad", Toast.LENGTH_LONG).show();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setTitle("Eliminar");
            dialog.setMessage("SE ELIMINARAN EN CASCADA TODOS LOS REGISTROS RELACIONADOS. Realmente desea eliminar este Registro?");
            dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aceptar();
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar();
                }
            });
            dialog.show();
        }
    }

    public void aceptar() {
        String regEliminadas;
        Modalidad modalidad=new Modalidad();
        modalidad.setCodmodalidad(editcodModalidad.getText().toString());
        try {


            controlhelper.abrir();
            regEliminadas=controlhelper.eliminar(modalidad);
            controlhelper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            controlhelper.cerrar();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar() {
        finish();
    }


}