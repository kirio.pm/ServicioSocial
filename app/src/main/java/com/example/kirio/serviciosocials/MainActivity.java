package com.example.kirio.serviciosocials;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
public class MainActivity extends ActionBarActivity {

    String[] menu={"Proyectos","Tipo de Proyectos"," Entidades","Especialidades","Modalidad"
            ,"Tipo de Actividad"," Tutor"," Encargados Servicio Social"," Alumnos"," Alumnos por Proyecto"," Actividades","Mantenimiento Base de Datos"};

    String[] activities={"ProyectoMenuActivity","TipoProyectoMenuActivity","EntidadMenuActivity","EspecialidadMenuActivity","ModalidadMenuActivity",
            "TipoActividadMenuActivity","TutorMenuActivity","EncargSActivity","MainAlumno","MainAlumnoProyecto","MenuActividad"};
    ControlBD BDhelper;

    ListView listaMenu;
    AdapterView<?> adapter;
    private Context context;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BDhelper=new ControlBD(this);
        context=getApplicationContext();
        listaMenu=(ListView)findViewById(R.id.listView3);
        ArrayAdapter<String> adapter=new ArrayAdapter(context,android.R.layout.simple_list_item_1,menu);
        listaMenu.setAdapter(adapter);
        listaMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != 11) {
                    nexActivity(activities[position]);


                } else {
                    GuardarBase();
                }

            }
        });

    }


    public void nexActivity(String layount){
        try {

            Class<?> clase = Class.forName("com.example.kirio.serviciosocials." + layount);
            Intent intent = new Intent(getApplicationContext(), clase);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            Toast.makeText(this, "ClassNotFount!!!", Toast.LENGTH_LONG).show();
        }

    }



    //metodos para seleccionar opcion


    public void GuardarBase(){
        //BDhelper=new ControlBD(this);
        BDhelper.abrir();
        String tost=BDhelper.llenarBD();
        BDhelper.cerrar();

        Toast.makeText(context, tost, Toast.LENGTH_SHORT).show();

    }
    public void Cerrar(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setIcon(R.drawable.ic_salir);
        dialog.setTitle("Cerrar Sesión");
        dialog.setMessage("¿Desea cerrar sesion?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                MainActivity.this.finish();

            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.show();
    }
}
