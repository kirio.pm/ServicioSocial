package com.example.kirio.serviciosocials;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.SQLException;

public class TipoActividadEliminarActivity extends Activity {
    EditText editcodTipoActividad,editcodModalidad;
    ControlBD controlhelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_actividad_eliminar);
        controlhelper=new ControlBD(this);
        editcodTipoActividad=(EditText)findViewById(R.id.editcodigoTipoActividad);
        editcodModalidad=(EditText)findViewById(R.id.editcodModalidad);

    }

    public void eliminarTipoActividad(View v) throws SQLException {
        String vacio="";
        String ed1=editcodTipoActividad.getText().toString();
        String ed2=editcodModalidad.getText().toString();

        if(vacio.equalsIgnoreCase(ed1) || vacio.equalsIgnoreCase(ed2)) {
            Toast.makeText(this, "Ingrese el codigo del tipo de actividad", Toast.LENGTH_LONG).show();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setTitle("Eliminar");
            dialog.setMessage("SE ELIMINARAN EN CASCADA TODOS LOS REGISTROS RELACIONADOS. Realmente desea eliminar este Registro?");
            dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aceptar();
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar();
                }
            });
            dialog.show();
        }
    }

    public void aceptar() {
        String regEliminadas;

        TipoActividad tipo=new TipoActividad();
        tipo.setCodtipoactividad(editcodTipoActividad.getText().toString());
        tipo.setCodmodalidad(editcodModalidad.getText().toString());
        try {
            controlhelper.abrir();
            regEliminadas=controlhelper.eliminar(tipo);
            controlhelper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            controlhelper.cerrar();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar() {
        finish();
    }





    public void limpiar(View v){
        editcodTipoActividad.setText("");
        editcodModalidad.setText("");

    }
}