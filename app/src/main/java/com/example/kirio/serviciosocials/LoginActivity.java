package com.example.kirio.serviciosocials;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import android.view.View;

import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	final static String infoLogTipoU = "com.example.kirio.serviciosocials.MainActivity";
    private EditText inputEmail;
    private EditText inputPassword;
    ControlBD BDhelper;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        inputEmail = (EditText) findViewById(R.id.txtEmail);
        inputPassword = (EditText) findViewById(R.id.txtPass);
		BDhelper= new ControlBD(this);
       
      	 //Toast.makeText(this, tost, Toast.LENGTH_SHORT).show(); */
       // loginErrorMsg = (TextView) findViewById(R.id.login_error);
    }
        
        
 
            public void Login(View v){
                String user = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();
                BDhelper.abrir();
      	        Usuario usuario=BDhelper.consultarUsuario(user);
      	        BDhelper.cerrar();
      	        
      	        if(usuario == null) {
      	        	
      	    	  Toast.makeText(this, "Usuario  " + user + " no se encontro", Toast.LENGTH_LONG).show();}
      	      else{ 
      	    	   String tpu=usuario.getidtp();
     	    	   if (password.equals(usuario.getPass())){
    	    		   if(tpu.equals("1")||tpu.equals("5") ){
    	    		      Intent intent = new Intent(LoginActivity.this, MainActivity.class);
    	    		      intent.putExtra("tipo",tpu);//le envio el parametro del tipo de usuario a la siguiente actividad
    	    		      intent.putExtra("nombreU", usuario.getNameUser());
    	                  startActivity(intent);
    	                  finish();}
    	    		   else{
    	    			  Intent intent = new Intent(LoginActivity.this, PantallaUsuarioActivity.class);
    	    			  intent.putExtra("tipo",tpu);
    	    			  intent.putExtra("nombreU", usuario.getNameUser());
     	                  startActivity(intent);
     	                  finish();  
    	    		   }}
    	    		else   
    	    			Toast.makeText(this, "La Contrasena no coincide al registro", Toast.LENGTH_LONG).show();
    	    	   }
    	    		   
    	       }
    	    	   
                        
    
    public void Registrar(View v){
    	Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
        startActivity(intent);
        finish();
    	
    }
               
 
}
