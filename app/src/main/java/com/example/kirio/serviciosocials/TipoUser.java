package com.example.kirio.serviciosocials;


public class TipoUser {

    private String idtipo;
    private String nombretipo;

    public TipoUser(){

    }
    public TipoUser(String nombre){
        super();
        this.nombretipo = nombre;
    }

    public TipoUser(String idtipo, String nombretipo) {
        super();
        this.idtipo = idtipo;
        this.nombretipo = nombretipo;
    }

    public String getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(String idtipo) {
        this.idtipo = idtipo;
    }

    public String getNombretipo() {
        return nombretipo;
    }

    public void setNombretipo(String nombretipo) {
        this.nombretipo = nombretipo;
    }
    @Override
    public String toString(){
        return this.nombretipo;

    }

}
