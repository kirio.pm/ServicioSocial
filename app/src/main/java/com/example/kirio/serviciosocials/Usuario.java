package com.example.kirio.serviciosocials;


public class Usuario  {

    private String nameUser;
    private String idtp;
    private String nombre;
    private String apellido;
    private String pass;


    public Usuario(){

    }
    public Usuario(String nameUser,String idtp, String nombre, String apellido, String pass) {
        super();
        this.nameUser = nameUser;
        this.idtp= idtp;
        this.nombre = nombre;
        this.apellido = apellido;
        this.pass = pass;
    }
    public String getNameUser() {
        return nameUser;
    }
    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }
    public String getidtp() {
        return idtp;
    }
    public void setidtp(String idtp) {
        this.idtp = idtp;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }






}

