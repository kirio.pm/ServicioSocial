package com.example.kirio.serviciosocials;


import java.util.ArrayList;


import com.example.kirio.serviciosocials.ControlBD;
import com.example.kirio.serviciosocials.Usuario;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegistroActivity extends Activity {
	
	EditText editUsuario; 
	EditText editContrasena;
	EditText editContrasena2;
	EditText editNombre;
	EditText editApellido;
	//EditText editCorreo;
	Spinner  spnTipoUser;
	TextView textpasw;
	Boolean vld,vld2;
	ArrayList<String> tipo = new ArrayList<String>();
	String user,pass,pass2,name,apell,tipoU;
	ControlBD bdhelper; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		bdhelper= new ControlBD(this);
		editUsuario=(EditText) findViewById(R.id.editusuario); 
		editContrasena=(EditText) findViewById(R.id.editpass);
		editContrasena2=(EditText) findViewById(R.id.editpass2);
		editNombre = (EditText) findViewById(R.id.editNombre);
		editApellido =(EditText) findViewById(R.id.editApellido);
		editNombre.setHint("Su Nombre Aqui");
		editApellido.setHint("Su Apellido Aqui");
		//editCorreo =(EditText) findViewById(R.id.editemail);
		textpasw = (TextView) findViewById(R.id.textpass); 
		spnTipoUser = (Spinner) findViewById(R.id.spinnerCargo);
		llenarSpinner();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.registro, menu);
		return true;
	}
	
	public void RegistrarUsuario (View v){
		textpasw.setText("");
		 user = editUsuario.getText().toString();
		 pass = editContrasena.getText().toString();
		pass2 = editContrasena2.getText().toString();
		name = editNombre.getText().toString();
		apell = editApellido.getText().toString();
		//ring email = editCorreo.getText().toString();
		tipoU= spnTipoUser.getSelectedItem().toString();
		vld=valueRegistro(user,pass,pass2,name,apell);
		
		
	       if(vld==true){
	    	  insertar();				
		Toast.makeText(this, "Usted se ha registrado como "+user+" su cargo es: " +tipoU , Toast.LENGTH_LONG).show();}
	}
		
			
	private void insertar(){
		String idt=null;
		
		if(tipoU.equals("Administrador"))
				idt="1";
		else
			if(tipoU.equals("Director")) idt="2";
			else
				if(tipoU.equals("Docente")) idt="3";
		       else
		        if(tipoU.equals("Estudiante"))idt="4";
		        else
		        	if(tipoU.equals("Administrador"))idt="5";
		 
			
		
		Usuario usuario=new Usuario(); 
		usuario.setNameUser(user);
		usuario.setidtp(idt);
		usuario.setNombre(name);
		usuario.setApellido(apell);
		usuario.setPass(pass);
		bdhelper.abrir(); 
		String regInsertados=bdhelper.insertarUsuario(usuario); 
		bdhelper.cerrar(); 
		Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show(); 
		 
	}
	
	
	
	public boolean  valueRegistro (String u,String p,String ps,String n,String a){
		 
		
		if(!(IsVacio(u)) && !(IsVacio(p)) && !(IsVacio(ps)) && !(IsVacio(n)) && !(IsVacio(a))){
			if(IsAlfaNumEsp(u)){
			if (p.equals(ps)){
	    	  if(IsName(n) && IsName(a)){
	    		  Toast.makeText(this, "registro valido", Toast.LENGTH_LONG).show();
	    		  return true;
	    	  }
	    	  else{
	    		  Toast.makeText(this, "Formato de Nombre o Apellido no valido "+" debe ingresar: mayuscula y minuscula "+
	    	                     " respetando espacio en blanco  ", Toast.LENGTH_LONG).show(); 
	    		  editNombre.setText("");
	    		  editApellido.setText("");
	    		  return false;
	    	  }}
	    	 else{
				  textpasw.setText("No Coinciden");
				  textpasw.setTextColor(Color.RED);
				  textpasw.setTypeface(null, Typeface.BOLD);
				  Toast.makeText(this, "Contrase�as diferentes", Toast.LENGTH_LONG).show();
				  return false;
				 
		       }}
		  else{
			Toast.makeText(this, "El campos Usuario no acepta espacios", Toast.LENGTH_LONG).show();
			return false;
		   }}
		  else{
			Toast.makeText(this, "Exiten campos vacios", Toast.LENGTH_LONG).show();
			return false;
		}
		
	}
	// metodos de validacion de datos
	public static boolean IsVacio(String text){
		boolean sies=true;
		  
			  if ((text==null) || (text.equals("")))
		  	  sies=true;
		  else
			  sies=false;
		return sies;
	}
	
	public static boolean IsName(String text){
		boolean sies=true;
		   if (text.matches("[A-Z][a-z ]*([A-Z][a-z]*){0,1}"))
		  	sies=true;
		  else
			  sies=false;
		return sies;
	}
	public static boolean IsAlfaNumEsp(String text){
		boolean sies=true;
		  if (text.matches("[a-zA-z0-9_]*"))
			  sies=true;
		  else
			  sies=false;
		return sies;
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		  if (keyCode == KeyEvent.KEYCODE_BACK){
			  Intent intent = new Intent(RegistroActivity.this,LoginActivity.class);
              startActivity(intent);
              finish();}
		  
			  
		return false;
	    }
   //llenar Spinner
	private void llenarSpinner(){
		 try{
	        	bdhelper.abrir();
	        	Cursor crs= bdhelper.db.rawQuery("Select tipou From TipoUsuario", null);
	            String obj;
	    		while(crs.moveToNext()){
	    			obj= crs.getString(0);
	    			if(!obj.equals("Administrador"))
	    			  tipo.add(obj);
	    	 		}
	    		ArrayAdapter<String> adaptador= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,tipo);
	    		spnTipoUser.setAdapter(adaptador);
	        }catch(Exception e){
	        	
	        }
	}
}
