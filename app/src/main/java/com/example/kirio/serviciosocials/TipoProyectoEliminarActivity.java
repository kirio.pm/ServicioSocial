package com.example.kirio.serviciosocials;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TipoProyectoEliminarActivity extends Activity {

    EditText editCodigoTipoProyecto;
    ControlBD controlhelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_proyecto_eliminar);
        controlhelper=new ControlBD (this);
        editCodigoTipoProyecto=(EditText)findViewById(R.id.editCodigoTipoProyecto);
    }
    public void eliminarTipoProyecto(View v){
        String vacio="";
        String ed1=editCodigoTipoProyecto.getText().toString();
        if(vacio.equalsIgnoreCase(ed1)) {
            Toast.makeText(this, "Ingrese tipo proyecto", Toast.LENGTH_LONG).show();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setTitle("Eliminar");
            dialog.setMessage("SE ELIMINARAN EN CASCADA TODOS LOS REGISTROS RELACIONADOS. Realmente desea eliminar este Registro?");
            dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aceptar();
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar();
                }
            });
            dialog.show();
        }
    }


    public void aceptar() {
        String regEliminadas;
        TipoProyecto tp=new TipoProyecto();
        tp.setCodigoTipoProyecto(editCodigoTipoProyecto.getText().toString());
        try {
            controlhelper.abrir();
            regEliminadas=controlhelper.eliminar(tp);
            controlhelper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            controlhelper.cerrar();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar() {
        finish();
    }



}