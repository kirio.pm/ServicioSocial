package com.example.kirio.serviciosocials;


import java.util.ArrayList;

import com.example.kirio.serviciosocials.ControlBD;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;





@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("ShowToast")
public class FragmentPerfil extends Fragment {

    TextView txt1,txt2,txt3;
    public String el_user="admin";
    EditText contenedor;
    Button back,next,CP,D;
    int id_user=1;
    ControlBD BDhelper;
    MainActivity uadm;
    String [] Tipo={"Administrador","Director","Docente","Estudiante","Gestor"};
    ArrayList<String> consulta; //arraylist con los usuarios extraidos de la base de datos
	//Controladores para el anterior y el siguiente
	int posicion=0; //Posicion actual en el arreglo de usuarios
	int tamano; //Tamano del arreglo de usuarios
	//El fragment se ha adjuntado al Activity
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
     
    //El Fragment ha sido creado        
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     // Create the list fragment and add it as our sole content.
        BDhelper= new ControlBD(getActivity());
        
        
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.cuenta_seccion, container, false);
        if(rootView != null){
        	//se cargan todas las vista
        	txt1 = (TextView) rootView.findViewById(R.id.text_cuenta1);
        	txt2 = (TextView) rootView.findViewById(R.id.text_cuenta2);
        	txt3 = (TextView) rootView.findViewById(R.id.text_cuenta3);
        	contenedor=(EditText) rootView.findViewById(R.id.editGstuser);
        	pasarDatos();
        	 back =(Button) rootView.findViewById(R.id.backbtn);
        	 next =(Button)rootView.findViewById(R.id.nextbtn);
        	 CP=(Button) rootView.findViewById(R.id.buttonCreateU);
        	 D=(Button) rootView.findViewById(R.id.buttonDeleteU);
        	 //D.setVisibility(View.INVISIBLE);
        	 iniciaContenedor();
			//Eventos onClick para cada boton
			next.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					posicion+=1;
					if (tamano > posicion)
					{						
						contenedor.setText(consulta.get(posicion));
						el_user=consulta.get(posicion);
					}					
					else 
						{
						posicion-=1;
					
						}
				}	        
	        });
			back.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					posicion-=1;
					if (posicion >= 0)
					{
						
						contenedor.setText(consulta.get(posicion));
						el_user=consulta.get(posicion);
					}
						
					else 
						{
						posicion+=1;
						
						}
				}	        
	        });
			D.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String us=contenedor.getText().toString();
			    	String toast=BDhelper.eliminarUsuario(us);
			    	Toast.makeText(getActivity(), toast, Toast.LENGTH_LONG).show();
			    	iniciaContenedor();
				}	        
	        });
			
			CP.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					registerForContextMenu(CP);//le asigno un menu contextual a este boton
					
			    }	        
	        });
        }
        return rootView;
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu,View v, ContextMenuInfo menuInfo){
    	//cargo el menu que deseo mostrar desde un .xml
    	menu.setHeaderTitle("Asignar Privilegios como:");
    	menu.setHeaderIcon(R.drawable.ic_usuario);
    	getActivity().getMenuInflater().inflate(R.menu.menu_asigna, menu); 
       	    	   	 
     }
    //si selecciono un item de mi menu que hace
    @Override 
    public boolean onContextItemSelected ( MenuItem item ) { 
    	//String s_tipo=null;
        switch ( item.getItemId ()) { 
            case R.id.u_di:
            	privilegios("2");
            	break;
            case R.id.u_do: 
            	 	privilegios("3");
            	 	break;
            case R.id.u_est: 
            	   	privilegios("4");
            	   	break;
            case R.id.u_gest: 
            	privilegios("5");
            	break;
                
        } 
        return super .onContextItemSelected ( item ) ; 
    } 
 
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
    }
        //cargo los datos del usuario que deseo mostrar
        private void pasarDatos(){
        	try{
            	BDhelper.abrir();
            	Cursor crs= BDhelper.db.rawQuery("Select * From Usuario WHERE nombreuser='admin'", null);
                
        		while(crs.moveToNext()){
        			txt1.setText("Usuario: "+crs.getString(0));
        			int tpu=Integer.parseInt(crs.getString(1)); 
        			tpu=tpu-1;
        			txt3.setText("Cargo: "+Tipo[tpu]);
        			String s=("Nombre: "+crs.getString(2))+" "+(crs.getString(3));
        			txt2.setText(s);
        		}
            }catch(Exception e){
            	
            }
         }
        
        private void iniciaContenedor(){
    		consulta=BDhelper.getUsuarioBD();
			tamano=consulta.size();
			contenedor.setText(consulta.get(0));
    	}
        
        private void privilegios(String s_id){
        	String us=contenedor.getText().toString();
	    	String toast=BDhelper.actualizarTypeUser(us,s_id);
	    	Toast.makeText(getActivity(), toast+"usuario ="+s_id, Toast.LENGTH_LONG).show();
        }
        
         
    
    } 

