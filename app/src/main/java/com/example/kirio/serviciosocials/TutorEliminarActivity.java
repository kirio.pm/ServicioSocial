package com.example.kirio.serviciosocials;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TutorEliminarActivity extends Activity {
    EditText editCodtutor, editCodespecial;
    ControlBD controlhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_eliminar);
        controlhelper=new ControlBD(this);
        editCodtutor=(EditText)findViewById(R.id.editCodtutor);
        editCodespecial=(EditText)findViewById(R.id.editCodespecial);
    }
    public void eliminarTutor(View v){
        String regEliminadas;
        String codt=editCodtutor.getText().toString();
        String code=editCodespecial.getText().toString();

        //Verificar que no haya dato vacio
        if (TextUtils.isEmpty(codt)|| TextUtils.isEmpty(code)) {
            Toast.makeText(this, "Por favor llene los campos vacios", Toast.LENGTH_LONG).show();
        }
        //fin verificar vacio
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setTitle("Eliminar");
            dialog.setMessage("SE ELIMINARAN EN CASCADA TODOS LOS REGISTROS RELACIONADOS. Realmente desea eliminar este Registro?");
            dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aceptar();
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelar();
                }
            });
            dialog.show();
        }
    }
    public void aceptar() {
        String regEliminadas;
        String codt=editCodtutor.getText().toString();
        String code=editCodespecial.getText().toString();
        Tutor tt = new Tutor();
        tt.setCodtutor(codt);
        tt.setCodespecial(code);
        try {
            controlhelper.abrir();
            regEliminadas = controlhelper.eliminar(tt);
            controlhelper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            controlhelper.cerrar();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar() {
        finish();
    }





    public void limpiar(View v){
        editCodtutor.setText("");
        editCodespecial.setText("");
    }
}